-- Lua script of map _stage/evolv.
-- This script is executed every time the hero enters this map.

local map = ...
local game = map:get_game()
local hero = map:get_hero()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  local hermione_asleep = game:get_value("hermione_asleep")
  local beaver_follower = hero:get_npc_follower() == "animals/beaver"
  hermione:get_sprite():set_animation("sleeping")

  if beaver_follower and not hermione_asleep then
    hermione_sensor:set_enabled()
  elseif hermione_asleep then
    hermione:set_enabled()
  end
end

-- Interaction with Neperforto's bell
function neperforto_bell:on_interaction()
  game:start_dialog("evolv.rachel.1")
  neperforto:get_sprite():set_direction(0)
end

function hermione_sensor:on_activated()
  self:set_enabled(false)

  local npc_follower = map:get_entity("npc_follower")
  if npc_follower then
    hero:freeze()
    map:get_camera():focus_on(npc_follower, 160)
    game:start_dialog("evolv.hermione.1", function()
      npc_follower:get_sprite():set_animation("walking")
      local m = sol.movement.create("target")
      m:set_target(hermione)
      m:set_speed(40)
      m:set_ignore_obstacles()
      m:start(npc_follower, function()
        hero:remove_npc_follower()
        hermione:set_enabled()
        game:start_dialog("evolv.hermione.2", function()
          game:set_value("hermione_asleep", true)
          map:get_camera():focus_on(hero, 160)
          hero:unfreeze()
        end)
      end)
    end)
  end
end
