-- Lua script of item shovel.

local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_savegame_variable("shovel")
  self:set_assignable(true)
end

-- Called when the item button is held.
function item:on_command_pressed(command)
  local hero = game:get_hero()
  local map = game:get_map()
  if hero:get_state() ~= "free" then
    return
  end
  hero:freeze() -- prevent the hero from moving with the shovel
  hero:set_animation("digging")
  local x, y, layer = hero:get_position()
  x = x + 8 - x%16
  y = y + 16 - y%16
  sol.timer.start(self, 500, function()
    log("Creating dug ground in " .. x .. ", " .. y)
    map:create_custom_entity({direction=0, layer=layer, x=x, y=y, width=16, height=16, sprite="entities/dug_ground", model="dug_ground"})
    for entity in map:get_entities_in_rectangle(x, y, 16, 16) do
      if not entity:is_enabled() then
        local xx, yy, zz = entity:get_position()
        log("Enabling entity in " ..  xx .. ", " .. yy)
        entity:set_enabled()
      end
    end
  end)
end

-- Called when the item button is released.
function item:on_command_released(command)
  local hero = game:get_hero()
  if hero:get_animation() ~= "digging" then
    return
  end
  -- Avoid Rachel to success if she did not dig enough
  sol.timer.stop_all(self)
  hero:unfreeze()
  hero:set_animation("stopped")
  -- Release the hero when you have finished.
  item:set_finished()
end
