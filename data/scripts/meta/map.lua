require("scripts/utils")
local tile_puzzle = require("scripts/tile_puzzle")
local darkness = require("scripts/menus/darkness")
local night = require("scripts/menus/night")
local map_meta = sol.main.get_metatable("map")

-- Return a list of puzzles on the map
function map_meta:get_puzzles()
  return self.tile_puzzles -- set by: entities/maze_tile.lua
end

-- Get a puzzle if it exists, or create one with the given name if not
function map_meta:get_puzzle(name)
  self.tile_puzzles[name] = self.tile_puzzles[name] or tile_puzzle:new()
  return self.tile_puzzles[name]
end

-- Destroy all puzzles associated with this map
function map_meta:clear_puzzles()
  self.tile_puzzles = {}
  log("Puzzles destroyed from map.")
end

-- Destroy puzzles by default when map is left
function map_meta:on_finished()
  self:clear_puzzles()
end

-- returns true if the map currently has trash entities on it
function map_meta:has_trash()
  -- Name of the savegame variable for this map and model
  local valname = string.format("entitystate__%s__%s", path_encode(self:get_id()), "trash")
  local trash_state_str = self:get_game():get_value(valname)

  for i = 1, #trash_state_str do
    local c = trash_state_str:sub(i, i)
    if c == "1" then
      return true -- 1 represents uncleaned trash. a single 1 means we have trash.
    end
  end
  return false
end

-- Called whenever a trash entity (from this map) is destroyed
function map_meta:on_trash_removed(trash_entity)
  if self:has_trash() == false then
    -- Trash is cleaned, roll the credits
    -- TODO: Do something.
  end
end

-- Create darkness around the hero
function map_meta:start_darkness()
  if not sol.menu.is_started(darkness) then
    sol.menu.start(self, darkness)
    sol.menu.bring_to_back(darkness)
  end
end

-- Remove darkness around the hero
function map_meta:stop_darkness()
  if sol.menu.is_started(darkness) then
    sol.menu.stop(darkness)
  end
end

-- Starts night
function map_meta:start_night()
  if not sol.menu.is_started(night) then
    sol.menu.start(self, night)
    sol.menu.bring_to_back(night)
  end
end

-- Stops night
function map_meta:stop_night()
  if sol.menu.is_started(night) then
    sol.menu.stop(night)
  end
end

-- Returns a list iterator of custom entities of the given model
function map_meta:get_entities_by_model(model)
  assert(type(model) == "string", "Model must be a string, not a "..type(model))
  assert(sol.main.resource_exists("entity", model), "Invalid entity type: "..model)

  local entities = {}
  for entity in self:get_entities_by_type("custom_entity") do
    if entity:get_model() == model then
      table.insert(entities, entity)
    end
  end

  return list_iter(entities)
end

-- Return a list iterator of torches in the same region than the given entity
function map_meta:get_torches_in_region(entity)
  local torches = {}
  if entity:get_type() == "custom_entity" and entity:get_model() == "torch" then table.insert(torches, entity) end
  for entity_i in self:get_entities_in_region(entity) do -- loop ALL OTHER entities
    if entity_i:get_type() == "custom_entity" and entity_i:get_model() == "torch" then
      table.insert(torches, entity_i)
    end
  end
  return list_iter(torches)
end
