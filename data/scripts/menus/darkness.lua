require("scripts/multi_events")
-- Simulates a dark cave

local darkness = {}

-- Prepare the surfaces
function darkness:on_started()
  local map = sol.main.game:get_map()
  local hero = map:get_hero()
  local hero_sprite = hero:get_sprite()

  self.torch_outline = sol.sprite.create("entities/torch")
  self.torch_outline:set_animation("unlit_darkness")
  self.chest_outline = sol.sprite.create("entities/chest")
  self.chest_outline:set_animation("closed_darkness")

  -- Make the outlined sprite mirror the hero
  self.hero_outline = sol.sprite.create("hero/tunic1-darkness")
  darkness.hero_outline:set_animation(hero_sprite:get_animation())
  darkness.hero_outline:set_frame(hero_sprite:get_frame())
  darkness.hero_outline:set_direction(hero_sprite:get_direction())
  hero_sprite:register_event("on_animation_changed", function(self, animation)
    darkness.hero_outline:set_animation(animation)
  end)
  hero_sprite:register_event("on_frame_changed", function(self, animation, frame)
    darkness.hero_outline:set_frame(frame)
  end)
  hero_sprite:register_event("on_direction_changed", function(self, animation, direction)
    darkness.hero_outline:set_direction(direction)
  end)
  darkness.hero_outline:synchronize(hero_sprite)

  self.black_layer = sol.surface.create(map:get_size())
  self.outline_layer = sol.surface.create(map:get_size())

  self.spotlight = {}
  self.spotlight_black = {}
  local spotlight_sizes = {"32", "64", "128"}
  for k, size in ipairs(spotlight_sizes) do
    self.spotlight[size] = sol.sprite.create("menus/spotlight")
    self.spotlight_black[size] = sol.sprite.create("menus/spotlight_black")
    self.spotlight[size]:set_animation(size)
    self.spotlight_black[size]:set_animation(size)
  end

  -- White pixels become transparent
  self.black_layer:set_blend_mode("multiply")

  -- Black pixels become transparent
  self.outline_layer:set_blend_mode("add")
end

-- Draw spotlights on the surface
function darkness:_draw_spotlights(spotlights, dst_surface)
  local map = sol.main.game:get_map()
  local hero = map:get_hero()

  for entity in map:get_entities_by_model("torch") do
    local entity_x, entity_y = entity:get_position()
    -- Illuminate using a given radius
    local size = entity.light_radius
    if size then
      spotlights[size]:draw(dst_surface, entity_x, entity_y)
    end
  end

  -- When the hero is brandishing treasure, we need to see her
  if hero:get_state() == "treasure" then
    local hero_x, hero_y = hero:get_position()
    spotlights["64"]:draw(dst_surface, hero_x, hero_y-16)
  end
end

-- Make everything dark except a square around the hero
function darkness:on_draw(dst_surface)
  self.outline_layer:clear()

  local map = sol.main.game:get_map()
  local hero = map:get_hero()
  local camera = map:get_camera()

  local hero_x, hero_y = hero:get_position()
  local camera_x, camera_y = map:get_camera():get_position()

  self.black_layer:fill_color({0, 0, 0})
  -- self.spotlight["32"]:draw(self.black_layer, hero_x, hero_y)
  self.hero_outline:draw(self.outline_layer, hero_x, hero_y)

  self:_draw_spotlights(self.spotlight, self.black_layer)

  for entity in map:get_entities_by_model("torch") do
    if entity.is_lit == false then
      local entity_x, entity_y = entity:get_position()
      self.torch_outline:draw(self.outline_layer, entity_x, entity_y)
    end
  end

  for entity in map:get_entities_by_type("chest") do
    local animation = entity:get_sprite():get_animation().."_darkness"
    local entity_x, entity_y = entity:get_position()
    self.chest_outline:set_animation(animation)
    self.chest_outline:draw(self.outline_layer, entity_x, entity_y)
  end

  self:_draw_spotlights(self.spotlight_black, self.outline_layer)

  self.black_layer:draw(dst_surface, 0-camera_x, 0-camera_y)
  self.outline_layer:draw(dst_surface, 0-camera_x, 0-camera_y)
end

return darkness
